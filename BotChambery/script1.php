<?php 

// $adresse = "https://www.chambery.fr/365-les-cours.htm"; // adresse de la page à exploiter
// echo "$adresse <image>"; // afficher l'adresse
// $page = file_get_contents ($adresse); // récupérer le contenu de la page
// echo $page ; // afficher la page



//? GUZZLE
require "vendor/autoload.php"; 
use GuzzleHttp\Client; 
use Symfony\Component\DomCrawler\Crawler; 

$client = new GuzzleHttp\Client(); 

//! COURS

$res = $client->request('GET', 'https://www.chambery.fr/365-les-cours.htm'); 
$statusCode = $res->getStatusCode(); 
$domHead = $res->getHeader('content-type')[0]; 
$domBody = $res->getBody(); 

//? CRAWLER
$crawler = new Crawler((string) $domBody); 

$images = $crawler->filterXPath('//li[contains(@class, "item")]/a/img')->extract(array('src'));
foreach ($images as $image){
  var_dump ("https://www.chambery.fr/".($image));
}

$descriptions = $crawler->filterXPath('//p[contains(@class, "description")]')->extract(['_text']);
var_dump($descriptions);

//! ACTIVITES

$res = $client->request('GET', 'https://www.chambery.fr/490-les-musiques.htm'); 

$statusCode = $res->getStatusCode(); 
$domHead = $res->getHeader('content-type')[0]; 
$domBody = $res->getBody(); 

$crawler = new Crawler((string) $domBody); 

$images = $crawler->filterXPath('//li[contains(@class, "item")]/a/img')->extract(array('src'));
foreach ($images as $image){
  var_dump ("https://www.chambery.fr/".($image));
}

$descriptions = $crawler->filterXPath('//p[contains(@class, "description")]')->extract(['_text']);

var_dump($descriptions);

// print_r($images);

?>


