--sqlite



CREATE TABLE Image (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_date DATE,
    name TEXT NOT NULL,
    description TEXT,
    id_article INTEGER,
    FOREIGN KEY(id_article) REFERENCES Article(id)
    );


CREATE TABLE Article (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_date DATE,
    titre TEXT NOT NULL,
    website_url TEXT NOT NULL,
    description TEXT
    );







